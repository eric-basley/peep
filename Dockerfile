FROM node:11.10-alpine

RUN mkdir -p /opt
WORKDIR /opt

COPY config /opt/config
COPY public /opt/public
COPY scripts /opt/scripts
COPY src /opt/src

COPY build /opt/build
COPY node_modules /opt/node_modules
COPY dist /opt/dist
COPY .eslint* package.json yarn.lock /opt/

EXPOSE 80
CMD yarn dist:run

