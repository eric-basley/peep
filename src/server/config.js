import debug from 'debug';
import params from './params';
const loginfo = debug('peep:config');

const init = () => {
  console.log(params);
  loginfo(`running "${params.env}" env`);
  return Promise.resolve({ config: params });
};

export default init;
