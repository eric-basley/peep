const serverOptions = {
  auto_reconnect: true,
  poolSize: 10,
};

module.exports = {
  secretKey: process.env.SECRET_KEY,
  siteUrl: 'http://peep.redpelicans.com',
  compress: true,
  devtool: false,
  devServer: null,
  server: {
    host: process.env.SERVER_HOST,
    port: process.env.SERVER_PORT,
  },
  db: {
    replicaSet: {
      name: 'rs0',
      servers: [
        {
          host:
            'mongo-prod-mongodb-replicaset-0.mongo-prod-mongodb-replicaset.prod.svc.cluster.local',
          port: 27017,
          options: serverOptions,
        },
        {
          host:
            'mongo-prod-mongodb-replicaset-1.mongo-prod-mongodb-replicaset.prod.svc.cluster.local',
          port: 27017,
          options: serverOptions,
        },
        {
          host:
            'mongo-prod-mongodb-replicaset-2.mongo-prod-mongodb-replicaset.prod.svc.cluster.local',
          port: 27017,
          options: serverOptions,
        },
      ],
    },
    database: 'timetrack',
  },
};
