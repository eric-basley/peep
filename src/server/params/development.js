const server = { 
  host: process.env.SERVER_HOST, 
  port: process.env.SERVER_PORT, 
};
const serverUrl = `http://${server.host}:${server.port}`;
module.exports = {
  secretKey: process.env.SECRET_KEY,
  siteUrl: 'http://localhost:3000',
  proxy: {
    '/socket.io': {
      ws: true,
      target: serverUrl,
      secure: false,
    },
    '/api': {
      target: serverUrl,
      secure: false,
    },
  },
  server: server,
  db: {
    host: process.env.MONGO_HOST,
    port: 27017,
    options: {
      auto_reconnect: true,
      poolSize: 10,
      w: 1,
      strict: true,
      native_parser: true,
    },
    database: 'timetrack',
  },
};
